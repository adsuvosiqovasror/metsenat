from typing import Union

from django.db.models import Model, QuerySet, Q
# from django.db.models.fields.related_descriptors import ForwardManyToOneDescriptor
from pydantic import BaseModel

from base.orm import AllCustomRequest

orm = AllCustomRequest


def update_password(obj, password):
    obj.set_password(password)
    return


def update(obj: Model, payload: Union[BaseModel, dict]) -> Model:
    if type(payload) is not dict:
        payload = payload.dict(exclude_none=True)
    # update_fields = obj.update_fields(obj)
    # if hasattr(obj, attr) and attr in update_fields and type(new_value) is not dict:
    for attr, new_value in payload.items():
        if hasattr(obj, attr) and type(new_value) is not dict:
            if attr == "password":
                update_password(obj, new_value)
                continue
            setattr(obj, attr, new_value)
        elif hasattr(obj, attr) and type(new_value) is dict:
            _update_fields_foreign_key_or_one_to_one(obj, attr, new_value)
    obj.save()
    return obj


def _update_fields_foreign_key_or_one_to_one(obj, fk_attr, payload: dict):
    fk_obj = getattr(obj, fk_attr)
    # update_fields = fk_obj.update_fields(obj)
    # if hasattr(fk_obj, attr) and attr in update_fields:
    for attr, value in payload.items():
        if hasattr(fk_obj, attr):
            if attr == "password":
                update_password(fk_obj, value)
                continue
            setattr(fk_obj, attr, value)
    fk_obj.save()


def remove_null_from_Q_object(q_data):
    """
    задача этой функции удалить все Q объекты, где значение атрибута Q, равна на None Q(attr=None)
    result: если все атрибуты Q равны на None Q(attr=None), то вернет пустой tuple и False
     иначе если хоть 1 атрибут Q не равно на None, то он вернет объект Q и True
     True и False нужен узнасть чтоб для фильтрации, надо-ли фильтровать специально под Q объектов
    """
    for num in range(len(q_data) - 1, -1, -1):
        if type(q_data.children[num]) is tuple:
            if type(q_data.children[num][1]) is type(None):
                del q_data.children[num]
        elif type(q_data.children[num]) is Q:
            remove_null_from_Q_object(q_data.children[num])
    if len(q_data) == 0:
        return tuple(), False
    return q_data, True


def add_count_objects(data: QuerySet, count: int) -> list:
    """
    data: это либо отфилтрованный(.filter()) или целиком взятые(.all()) объекты
    result: вернет либо общее кол-во из data или кол-во сущностей из БД, но если общее кол-во из data равна 0, то ключ
     count не будет добавиться в результат
    """
    list_data: list = list(data)
    if count != 0 and len(list_data) > 0:
        list_data.append({"count": count})
    return list_data


def check_paginate(
        model: Model,
        page: Union[int, None],
        fd_kwargs: Union[dict, None] = dict(),
        filter_data_args: Union[tuple, None] = tuple(),
        q_filtered: bool = False,
        page_size: int = 15,
        *args,
        **kwargs
) -> AllCustomRequest:
    """
    page: страница пагинации
    fd_kwargs: данные для фильтрации в виде словаря
    filter_data_args: данные для фильтрации в виде tuple
    q_filtered: если filter_data_args=(Sum(...), ...) и в args есть Q()-объект(ы) то ставить q_filtered=True
    page_size: кол-во объектов в 1 запроск, поумолчанию=15
    result: проверить наличие фильтрции и пагинации, и исходя от этого делать различные проаерки и выводы
    """
    # если нет никмких параиетров, то вывести данных кол-вом с page_size и добавить общее кол-во объектов
    if page is None and bool(fd_kwargs) is False and bool(filter_data_args) is False:
        return orm(model, end=page_size, *args, **kwargs)
    # если есть что фильтровать,и нет страницы пагинации,то фильтровать и вывести объекты кол-вом указанном в page_size
    elif page is None and bool(fd_kwargs or filter_data_args) is True:
        return orm(model, fd_args=filter_data_args, fd_kwargs=fd_kwargs, end=page_size, q_filtered=q_filtered,
                   *args, **kwargs)
    # если есть данные для фильтрации и страница(page) то отфильтровать и передать объекты в соответсвии с page
    elif bool(fd_kwargs or filter_data_args) is True and page is not None:
        start = page_size * page
        end = start + page_size
        return orm(model, fd_kwargs=fd_kwargs, fd_args=filter_data_args, start=start, end=end,
                   q_filtered=q_filtered, *args, **kwargs)
    start = page_size * page
    end = start + page_size
    return orm(model, start=start, end=end, *args, **kwargs)
