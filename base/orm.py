from typing import Optional, Union, List

from django.db.models import QuerySet, Model
from django.http import Http404
from django.shortcuts import get_object_or_404

from base.cache import AuthUserCache, GetSetDelaCacheANDUpdateVersion  # OLDVersionGetSetCacheData


# from base.search import DynamicFilter


class BaseCustomRequest(GetSetDelaCacheANDUpdateVersion, AuthUserCache):

    def __init__(
            self,
            model: Model,
            fd_kwargs: dict = dict(),  # fd_kwargs
            fd_args: tuple = tuple(),  # fd_kwargs
            annotate_data: Union[dict, None] = None,
            aggregate_data: Union[dict, None] = None,
            sr_fields: Union[tuple, None] = None,  # select_related_fields
            pr_fields: Union[tuple, None] = None,  # prefetch_related_fields
            last_func_bool: bool = False,
            first_func_bool: bool = False,
            start: Union[int, None] = None,
            end: Union[int, None] = None,
            q_filtered: bool = False,
            add_count_request: bool = False,
    ):
        self.model = model
        self.filter_data_kwargs = fd_kwargs
        self.filter_data_args = fd_args
        self.q_filtered = q_filtered
        self.annotate_data = annotate_data
        self.aggregate_data = aggregate_data
        self.select_related_fields = sr_fields
        self.prefetch_related_data = pr_fields
        self.last_func_bool = last_func_bool
        self.first_func_bool = first_func_bool
        self.start = start
        self.end = end
        self.add_count_request = add_count_request

    @property
    def count_paginate_all(self) -> int:
        count: int = self.model.objects.all().count()
        return count

    @property
    def count_paginate_filter(self) -> int:
        count: int = self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).count()
        return count

    @property
    def count_paginate_Q_filter(self) -> int:
        count: int = self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).count()
        return count

    @property
    def all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.all()[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.all()[self.start:]
        elif self.end is not None:
            return self.model.objects.all()[:self.end]
        return self.model.objects.all()

    @property
    def filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs)

    @property
    def annotate_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.all().annotate(**self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.all().annotate(**self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.all().annotate(**self.annotate_data)[:self.end]
        return self.model.objects.annotate(**self.annotate_data)

    @property
    def annotate_filtered(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).annotate(
                    **self.annotate_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).annotate(
                    **self.annotate_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).annotate(
                    **self.annotate_data)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).annotate(
                **self.annotate_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).annotate(
                **self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).annotate(
                **self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).annotate(
                **self.annotate_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).annotate(
            **self.annotate_data)

    @property
    def get(self):
        return get_object_or_404(self.model, *self.filter_data_args, **self.filter_data_kwargs)

    @property
    def get_annotate(self):
        try:
            return self.model.objects.annotate(**self.annotate_data).get(
                *self.filter_data_args, **self.filter_data_kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)


class SelectCustomRequest(BaseCustomRequest):
    @property
    def select_related_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.select_related(*self.select_related_fields)[self.start:]
        elif self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields)[:self.end]
        return self.model.objects.select_related(*self.select_related_fields)

    @property
    def select_related_and_annotate_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).annotate(
                **self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.select_related(*self.select_related_fields).annotate(
                **self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).annotate(
                **self.annotate_data)[self.start:self.end]
        return self.model.objects.select_related(*self.select_related_fields).annotate(**self.annotate_data)

    @property
    def select_related_and_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
            *self.select_related_fields)

    @property
    def select_related_and_annotate_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).annotate(**self.annotate_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).annotate(**self.annotate_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).annotate(**self.annotate_data)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).annotate(**self.annotate_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).annotate(**self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).annotate(**self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).annotate(**self.annotate_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
            *self.select_related_fields).annotate(**self.annotate_data)

    @property
    def select_related_and_get(self):
        try:
            return self.model.objects.select_related(*self.select_related_fields).get(
                *self.filter_data_args, **self.filter_data_kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)

    @property
    def select_related_and_annotate_get(self):
        try:
            return self.model.objects.select_related(*self.select_related_fields).annotate(**self.annotate_data).get(
                *self.filter_data_args, **self.filter_data_kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)


class PrefetchCustomRequest(BaseCustomRequest):

    @property
    def prefetch_related_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data)[self.start:self.end]
        return self.model.objects.prefetch_related(*self.prefetch_related_data)

    @property
    def prefetch_related_and_annotate_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[self.start:self.end]
        return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(**self.annotate_data)

    @property
    def prefetch_related_and_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
            *self.prefetch_related_data)

    @property
    def prefetch_related_and_annotate_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                    *self.prefetch_related_data).annotate(**self.annotate_data)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).prefetch_related(
            *self.prefetch_related_data).annotate(**self.annotate_data)

    @property
    def prefetch_related_get(self):
        try:
            return self.model.objects.prefetch_related(*self.prefetch_related_data).get(
                *self.filter_data_args, **self.filter_data_kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)

    @property
    def prefetch_related_and_annotate_get(self):
        try:
            return self.model.objects.annotate(**self.annotate_data).prefetch_related(
                *self.prefetch_related_data).get(*self.filter_data_args, **self.filter_data_kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)


class SelectANDPrefetchCustomRequest(BaseCustomRequest):

    @property
    def select_related_and_prefetch_related_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data)[self.start:self.end]
        return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
            *self.prefetch_related_data)

    @property
    def select_related_and_prefetch_related_and_annotate_all(self) -> QuerySet:
        if self.start is not None and self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
                *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]
        return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
            *self.prefetch_related_data).annotate(**self.annotate_data)

    @property
    def select_related_and_prefetch_related_and_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[:self.end]
            return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
            *self.select_related_fields).prefetch_related(*self.prefetch_related_data)

    @property
    def select_related_and_prefetch_related_and_annotate_filter(self) -> QuerySet:
        if self.q_filtered:
            if self.start is not None and self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                    **self.annotate_data)[self.start:self.end]
            elif self.start is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                    **self.annotate_data)[self.start:]
            elif self.end is not None:
                return self.model.objects.filter(self.filter_data_args, **self.filter_data_kwargs).select_related(
                    *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                    **self.annotate_data)[:self.end]
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)
        if self.start is not None and self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[self.start:self.end]
        elif self.start is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[self.start:]
        elif self.end is not None:
            return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
                *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
                **self.annotate_data)[:self.end]
        return self.model.objects.filter(*self.filter_data_args, **self.filter_data_kwargs).select_related(
            *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(**self.annotate_data)


class AllCustomRequest(SelectCustomRequest, PrefetchCustomRequest, SelectANDPrefetchCustomRequest):

    def __init__(self, *args, **kwargs):
        super(AllCustomRequest, self).__init__(*args, **kwargs)

    # @property
    # def limit_all(self) -> QuerySet:
    #     return self.model.objects.all()[:self.end]
    #
    # @property
    # def offset_all(self) -> QuerySet:
    #     return self.model.objects.all()[self.start:]
    #
    # @property
    # def paginate_all(self) -> QuerySet:
    #     if self.start is not None and self.end is not None:
    #         return self.model.objects.all()[self.start:self.end]
    #     elif self.start is not None:
    #         return self.model.objects.all()[self.start:]
    #     elif self.end is not None:
    #         return self.model.objects.all()[:self.end]

    # @property
    # def limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs)[:self.end]
    #
    # @property
    # def offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs)[self.start:]
    #
    # @property
    # def paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs)[self.start:self.end]

    # @property
    # def select_related_limit_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields)[:self.end]
    #
    # @property
    # def select_related_offset_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields)[self.start:]
    #
    # @property
    # def select_related_paginate_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields)[self.start:self.end]

    # @property
    # def select_related_and_annotate_limit_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).annotate(**self.annotate_data)[:self.end]
    #
    # @property
    # def select_related_and_annotate_offset_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).annotate(
    #         **self.annotate_data)[self.start:]
    #
    # @property
    # def select_related_and_annotate_paginate_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).annotate(
    #         **self.annotate_data)[self.start:self.end]

    # @property
    # def select_related_and_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields)[:self.end]
    #
    # @property
    # def select_related_and_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields)[self.start:]
    #
    # @property
    # def select_related_and_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields)[self.start:self.end]

    # @property
    # def select_related_and_annotate_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).annotate(**self.annotate_data)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).annotate(**self.annotate_data)[:self.end]
    #
    # @property
    # def select_related_and_annotate_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).annotate(**self.annotate_data)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).annotate(**self.annotate_data)[self.start:]
    #
    # @property
    # def select_related_and_annotate_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).annotate(**self.annotate_data)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).annotate(**self.annotate_data)[self.start:self.end]

    # @property
    # def prefetch_related_limit_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data)[:self.end]
    #
    # @property
    # def prefetch_related_offset_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data)[self.start:]
    #
    # @property
    # def prefetch_related_paginate_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data)[self.start:self.end]

    # @property
    # def prefetch_related_and_annotate_limit_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[:self.end]
    #
    # @property
    # def prefetch_related_and_annotate_offset_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[self.start:]
    #
    # @property
    # def prefetch_related_and_annotate_paginate_all(self) -> QuerySet:
    #     return self.model.objects.prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[self.start:self.end]

    # @property
    # def prefetch_related_and_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data)[:self.end]
    #
    # @property
    # def prefetch_related_and_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data)[self.start:]
    #
    # @property
    # def prefetch_related_and_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data)[self.start:self.end]

    # @property
    # def prefetch_related_and_annotate_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data).annotate(**self.annotate_data)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[:self.end]
    #
    # @property
    # def prefetch_related_and_annotate_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
    #
    # @property
    # def prefetch_related_and_annotate_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #             *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]

    # @property
    # def select_related_and_prefetch_related_limit_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data)[:self.end]
    #
    # @property
    # def select_related_and_prefetch_related_offset_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data)[self.start:]
    #
    # @property
    # def select_related_and_prefetch_related_paginate_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data)[self.start:self.end]

    # @property
    # def select_related_and_prefetch_related_and_annotate_limit_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[:self.end]
    #
    # @property
    # def select_related_and_prefetch_related_and_annotate_offset_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:]
    #
    # @property
    # def select_related_and_prefetch_related_and_annotate_paginate_all(self) -> QuerySet:
    #     return self.model.objects.select_related(*self.select_related_fields).prefetch_related(
    #         *self.prefetch_related_data).annotate(**self.annotate_data)[self.start:self.end]

    # @property
    # def select_related_and_prefetch_related_and_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[:self.end]
    #
    # @property
    # def select_related_and_prefetch_related_and_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:]
    #
    # @property
    # def select_related_and_prefetch_related_and_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(
    #             *self.prefetch_related_data)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data)[self.start:self.end]


    # @property
    # def select_related_and_prefetch_related_and_annotate_limit_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #             **self.annotate_data)[:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[:self.end]
    #
    # @property
    # def select_related_and_prefetch_related_and_annotate_offset_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #             **self.annotate_data)[self.start:]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[self.start:]
    #
    # @property
    # def select_related_and_prefetch_related_and_annotate_paginate_filter(self) -> QuerySet:
    #     if self.q_filtered:
    #         return self.model.objects.filter(self.filter_data_args, **self.fd_kwargs).select_related(
    #             *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #             **self.annotate_data)[self.start:self.end]
    #     return self.model.objects.filter(*self.filter_data_args, **self.fd_kwargs).select_related(
    #         *self.select_related_fields).prefetch_related(*self.prefetch_related_data).annotate(
    #         **self.annotate_data)[self.start:self.end]
