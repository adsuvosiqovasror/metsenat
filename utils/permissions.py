from django.db import models
from django.http import HttpResponseForbidden, HttpRequest

from utils.backends import JWTAuthentication, CookieAuthenticate


class BasePermission:

    def __init__(self, chek_permission: bool = False, request: HttpRequest = None, obj=None):
        self.chek_permission = chek_permission
        if self.chek_permission:
            self.__call__ = None
        self.check_object_permissions(request, obj)

    def __call__(self, request):
        request.user = JWTAuthentication()(request)
        return self.has_permission(request)

    def check_object_permissions(self, request, obj):
        return self.has_object_permission(request, obj)

    def has_permission(self, request):
        return True

    @classmethod
    def has_object_permission(cls, request, obj):
        return True


class RefreshAuth(BasePermission):

    def __call__(self, request):
        request.user = CookieAuthenticate()(request)
        return self.has_permission(request)

    def has_permission(self, request):
        return True


class IsAuthenticate(BasePermission):

    def has_permission(self, request):
        return True if bool(request.user) else False


class IsAllManagerAuth(BasePermission):

    def has_permission(self, request):
        return True if bool(request.user.permission <= 2 or request.user.is_authenticated) else False


# class IsTeacherAuth(BasePermission):
#
#     def has_permission(self, request):
#         return True if bool(request.user.permission <= 3 or request.user.is_authenticated) else False
#
#     @classmethod
#     def has_object_permission(cls, request, obj: models.Model):
#         try:
#             is_teacher = obj.group.teacher_id == request.user_id
#         except Exception:  # RelatedObjectDoesNotExist
#             raise HttpResponseForbidden(content="1212")
#         return True if bool(request.user.permission <= 2 or is_teacher) else False
#
#
# class IsAuthenticatedCustom(BasePermission):
#     def has_permission(self, request, view):
#         return True if bool(request.user and request.user.is_authenticated) else False
#
#     def has_object_permission(self, request, view, obj):
#         if request.method == 'GET' or obj.user.pk == request.user.pk or request.user.profile.permission <= 2:
#             return True
#         return False
#
#
# class IsBrigadierUser(BasePermission):
#     def has_permission(self, request, view):
#         return True if bool(request.user and request.user.is_authenticated) else False
#
#     def has_object_permission(self, request, view, obj):
#         if request.method == 'GET' or obj.author.pk == request.user.pk or request.user.profile.permission >= 2:
#             return True
#         return False
#
#
# class CheckObjectUserInUser(BasePermission):
#     def has_permission(self, request, view):
#         return True if bool(request.user and request.user.is_authenticated) else False
#
#     def has_object_permission(self, request, view, obj):
#         if request.method == 'GET' or obj.author.pk == request.user.pk or request.user.profile.permission <= 2:
#             return True
#         return False
