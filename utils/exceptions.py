# from django.core.exceptions import ValidationError
from ninja.errors import ValidationError
from django.http import Http404


class AuthenticationFailed(ValidationError):
    pass
