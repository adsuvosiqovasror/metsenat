from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.utils.translation import gettext_lazy as _
from ninja.errors import HttpError

from base.models import BaseModel, BandManager


# from django.contrib.postgres.fields.jsonb import JSONField
# orders = JSONField(default=list, blank=True, null=True)
# orders = serializers.ListField(child=serializers.JSONField())


class UserManager(BaseUserManager):
    def create_user(self, phone, password, **extra_fields):
        if phone is None:
            raise TypeError("Users must have a phone.")
        # if password is None:
        #     raise TypeError("Both password fields must be filled")
        if self.model.objects.filter(phone=phone):
            raise ValidationError(_('пользователь с номером %(phone) уже существует'), code='email exists',
                                  params={'phone': phone})
        user = self.model(phone=phone, **extra_fields)
        if password is not None:
            user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self.create_user(phone, password, **extra_fields)


Permissions = (
    (0, _("Admin")),
    (1, _("Investor")),
    (2, _("Student"))
)

InvestorType = (
    (0, _("Legal Entity")),
    (1, _("Natural Person"))
)

ModeratorType = (
    (0, _("In Moderation")),
    (1, _("Confirmed")),
    (2, _("Canceled")),
)


class UserModel(BaseModel, AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=155)
    last_name = models.CharField(max_length=155)
    phone = models.CharField(max_length=16, unique=True, db_index=True)
    permission = models.IntegerField(choices=Permissions, default=3)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    USERNAME_FIELD = 'phone'
    objects = UserManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def save(self, *args, **kwargs):
        if len(self.phone) < 12:
            raise HttpError(status_code=400,
                            message="min length phone 12. Example: (+998 99 999 99 99) or (998 99 999 99 99)")
        super(UserModel, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return f'ID: {self.pk}, username: {self.phone}'

    def get_info(self):
        return {'id': self.pk, 'phone': self.phone, 'first_name': self.first_name, 'last_name': self.last_name}


class UniversityModel(BaseModel):
    university_name = models.CharField(max_length=255)

    class Meta:
        verbose_name = "University"
        verbose_name_plural = "Universities"


class StudentTypeModel(BaseModel):
    title = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Student Type"
        verbose_name_plural = "Student Types"


class DefaultPricesModel(BaseModel):
    price = models.IntegerField()

    class Meta:
        verbose_name = "Default Price"
        verbose_name_plural = "Default Prices"


class InvestorModel(BaseModel):
    user = models.OneToOneField(UserModel, related_name="investor", on_delete=models.CASCADE)
    price = models.ForeignKey(DefaultPricesModel, related_name="investor", on_delete=models.SET_NULL, null=True)
    individual_price = models.IntegerField(null=True, default=0)
    organization_name = models.CharField(max_length=500, null=True)
    remainder = models.IntegerField(default=0)
    investor_type = models.IntegerField(choices=InvestorType, default=0)
    moderator = models.IntegerField(choices=ModeratorType, default=0)

    class Meta:
        verbose_name = "Investor"
        verbose_name_plural = "Investors"

    def save(self, *args, **kwargs):
        if self.individual_price > 0 and self.price_id is None:
            self.remainder = self.individual_price
        elif self.individual_price == 0 and self.price_id is not None and self.price_id > 0:
            self.remainder = self.price.price
        if self.investor_type == 0 and self.organization_name is None:
            raise HttpError(status_code=400, message="organization name required")
        elif self.individual_price == 0 and self.price_id is None:
            raise HttpError(status_code=400, message="individual_price and price fields, one of them must be filled")
        elif self.individual_price > 0 and self.price_id is not None and self.price_id != 0:
            raise HttpError(status_code=400, message="individual_price and price fields, one of them must be filled")
        elif self.individual_price > 0 and self.price_id == 0:
            self.price_id = None
        remainder = kwargs.pop("remainder", None)
        if remainder is not None:
            self.remainder = remainder
        super(InvestorModel, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return f'ID: {self.pk}, username: {self.user.phone}'


class StudentModel(BaseModel):
    user = models.OneToOneField(UserModel, related_name="student", on_delete=models.CASCADE)
    university = models.ForeignKey(UniversityModel, related_name="students", on_delete=models.SET_NULL, null=True)
    photo = models.ImageField(upload_to='profiles/%Y/%m/%d/', blank=True, null=True)
    tag = models.ForeignKey(StudentTypeModel, related_name="students", on_delete=models.SET_NULL, null=True)
    contract_amount = models.IntegerField(null=True, default=0)
    allotted_amount = models.IntegerField(null=True, default=0)
    investors = models.ManyToManyField(InvestorModel, related_name="students")

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"

    def save(self, *args, **kwargs):
        allotted_amount = kwargs.pop("allotted_amount", None)
        print(allotted_amount)
        if allotted_amount is not None:
            self.allotted_amount = allotted_amount
        super(StudentModel, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return f'ID: {self.pk}, username: {self.user.phone}'


class InvestingStudentModel(BaseModel):
    investor = models.ForeignKey(InvestorModel, related_name="investment", on_delete=models.SET_NULL, null=True)
    student = models.ForeignKey(StudentModel, related_name="investors_student", on_delete=models.SET_NULL, null=True)
    sum = models.IntegerField(choices=ModeratorType, default=0)

    class Meta:
        verbose_name = "Investing Student"
        verbose_name_plural = "Investing Students"

    def __str__(self):
        return f'ID: {self.pk}, username: {self.sum}'


class JwtModel(BaseModel):
    user = models.OneToOneField(UserModel, on_delete=models.CASCADE, related_name="jwt")
    access = models.TextField()
    refresh = models.TextField()

    class Meta:
        verbose_name = "User Token"
        verbose_name_plural = "User Tokens"

    def __str__(self):
        return f'ID: {self.pk}, username: {self.user.phone}'
