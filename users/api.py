import datetime as dt
import json
from typing import List, Union

from django.db.models import Q, Sum, Count
from django.db.models.functions import TruncMonth
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404

from ninja import Router, Query, Body
from ninja.errors import HttpError
from pydantic import PositiveInt

from base.utils import update, check_paginate, remove_null_from_Q_object, add_count_objects
from users import schemes
from users.models import UserModel, JwtModel, StudentModel, InvestorModel, UniversityModel, StudentTypeModel, \
    DefaultPricesModel, InvestingStudentModel
from utils.ninja_code_status import message_code_status
from utils.permissions import RefreshAuth, IsAuthenticate, IsAllManagerAuth
from utils.token import UpdateTokens
from base.orm import AllCustomRequest

app = Router()
orm = AllCustomRequest


# password1 = payload.pop('password1', False)
# password2 = payload.pop('password2', False)
# if not password2 or not password2 or password2 != password1:
#     return 400, {'message': 'passwords do not match'}
# payload['password'] = password1

def saved_investor(payload: dict):
    user_data = payload.pop("user")
    price_id = payload.get("price_id", None)
    if UserModel.objects.filter(phone=user_data['phone']).exists():
        raise HttpError(status_code=401, message='A user with the same phone already exists')
    elif not DefaultPricesModel.objects.filter(id=price_id).exists() and price_id is not None:
        raise HttpError(status_code=400, message="object DefaultPrices not found")
    user = UserModel(**user_data).save()
    return InvestorModel(user_id=user.pk, **payload).save()


def update_payload_investor(obj, payload: dict) -> dict:
    moderator = payload.get("moderator", 0)
    organization = payload.get("organization_name", None)
    investor_type = payload.get("investor_type", obj.investor_type)
    if obj.moderator == 0 and moderator > 0 and obj.user.password == "":
        if payload.get("password1", None) is None or payload.get("password2", None) is None:
            raise HttpError(status_code=400, message="when changing the moderator, password1 and password2 must be "
                                                     "filled in, since this user has not been moderated yet")
        payload["user"] = {"password": payload.get("password1")}
    if obj.investor_type != investor_type:
        if obj.investor_type == 0:
            payload["organization_name"] = None
        elif obj.investor_type == 1 and organization is None:
            raise HttpError(status_code=400, message="when changing the type of investor, for a legal entity, the "
                                                     "organization field is required")
    if obj.investor_type == 1 and organization is not None:
        raise HttpError(status_code=400, message="individuals cannot fill in the organization field")
    return payload


@app.post(
    "/login/",
    response={message_code_status: schemes.Message, 200: schemes.AccessTokenScheme},
    tags=["auth"])
def login(request, payload: schemes.LoginSchema, response: HttpResponse):
    payload = payload.dict()
    request.user = authenticate(phone=payload['phone'], password=payload['password'])
    if request.user:
        return UpdateTokens(request=request, response=response).render_access_refresh_token
    return 401, {'message': 'User not found!'}


@app.get(
    "/refresh/",
    response={message_code_status: schemes.Message, 200: schemes.AccessTokenScheme},
    tags=["auth"],
    auth=RefreshAuth())
def get(request, response: HttpResponse):
    return UpdateTokens(request, response).response


@app.get(
    "/logout/",
    response={message_code_status: schemes.Message},
    tags=["auth"],
    auth=IsAuthenticate())
def logout(request, response: HttpResponse):
    JwtModel.objects.filter(user_id=request.user.pk).delete()
    response.delete_cookie(key='_at')
    return {'message': 'logout'}


@app.post(
    "/investor/yur",
    response=schemes.InvestorSchemeOut,
    tags=["investor"]
)
def register_yur(request, payload: schemes.InvestorSchemeIn):
    if payload.organization_name is None:
        raise HttpError(status_code=400, message="attribute organization is required")
    return saved_investor(payload.dict())


@app.post(
    "/investor/fiz",
    response=schemes.InvestorSchemeOut,
    tags=["investor"]
)
def register_fiz(
        request,
        payload: schemes.InvestorSchemeIn,
):
    """
    **price**: enter price id\n
    **individual_price**: other amount\n
    **organization_name**: organization name\n
    """
    payload = payload.dict()
    if payload["organization_name"] is not None:
        del payload["organization_name"]
    payload["investor_type"] = 1
    return saved_investor(payload)


@app.get(
    "/investors",
    response=list[schemes.InvestorSchemeOut],
    tags=["investor"],
    exclude_unset=True
)
def get_investors(
        request,
        page: Union[int, None] = Query(default=None, description="enter the page number", ge=1),
        moderator: Union[int, None] = Query(
            default=None, ge=0, le=2, description="0: In Moderation, 1: Confirmed, 2: Canceled"),
        price_id: Union[int, None] = Query(default=None, description="enter id fixed prices"),
        individual_prices: Union[str, None] = Query(default=None, description="example: 15000-30000"),
        date_form: Union[dt.date, None] = Query(default=None, description="date is from"),
        date_to: Union[dt.date, None] = Query(default=None, description="date is to"),
        search: bool = Query(default=False,
                             description="if search is true then there will be a search and not a filtering")
):
    if search is True:
        args = (Q(moderator=moderator) | Q(price_id=price_id) | Q(individual_prices__range=individual_prices) |
                Q(created_at__date__range=(date_form, date_to)))
        filter_data_args, q_filtered = remove_null_from_Q_object(args)
        request_orm = check_paginate(InvestorModel, page=page, filter_data_args=filter_data_args, q_filtered=q_filtered,
                                     sr_fields=("user",))
        data = request_orm.select_related_and_filter
        return add_count_objects(data, request_orm.count_paginate_Q_filter)
    filter_data = schemes.InvestorsFilterScheme(
        moderator=moderator, price_id=price_id, prices=individual_prices, range_date=(date_form, date_to))
    filter_dict = filter_data.dict(exclude_none=True)
    request_orm = check_paginate(InvestorModel, page, filter_dict, sr_fields=("user",))
    data = request_orm.select_related_and_filter
    return add_count_objects(data, request_orm.count_paginate_filter)


@app.get(
    "/investors/pk",
    response=schemes.InvestorSchemeOut,
    tags=["investor"],
    exclude_unset=True
)
def get_investor(request, pk: int):
    return orm(InvestorModel, fd_kwargs={"pk": pk}).get


@app.patch(
    "/investors/yur/pk",
    response=schemes.InvestorSchemeOut,
    tags=["investor"],
    exclude_unset=True
)
def updates_investor_yur(request, pk: int, payload: schemes.InvestorSchemeUpdate):
    obj = orm(InvestorModel, fd_kwargs={"pk": pk}, sr_fields=("user",)).select_related_and_get
    payload = payload.dict(exclude_none=True, exclude_defaults=True)
    valid_payload = update_payload_investor(obj, payload)
    return update(obj, valid_payload)


@app.patch(
    "/investors/fiz/pk",
    response=schemes.InvestorSchemeOut,
    tags=["investor"],
    exclude_unset=True
)
def updates_investor_fiz(request, pk: int, payload: schemes.InvestorSchemeUpdate):
    obj = orm(InvestorModel, fd_kwargs={"pk": pk}, sr_fields=("user",)).select_related_and_get
    payload = payload.dict(exclude_none=True)
    valid_payload = update_payload_investor(obj, payload)
    return update(obj, valid_payload)


@app.delete(
    "/investors/pk",
    response=schemes.Message,
    tags=["investor"],
)
def delete_investor(request, pk: int, payload: schemes.InvestorSchemeIn):
    obj: InvestorModel = orm(InvestorModel, fd_kwargs={"pk": pk}).get
    obj.user.delete()
    obj.delete()
    return {"message": "success deleted"}


@app.post(
    "/university",
    response=schemes.UniversitySchemeOut,
    tags=["university"]
)
def create_university(request, payload: schemes.UniversitySchemeIn):
    return UniversityModel.objects.create(university_name=payload.university_name)


@app.get(
    "/universities",
    response=list[schemes.UniversitySchemeOut],
    tags=["university"]
)
def get_universities(request):
    return orm(UniversityModel).all


@app.get(
    "/university/pk",
    response=schemes.UniversitySchemeOut,
    tags=["university"]
)
def get_university(request, pk: int):
    return orm(UniversityModel, fd_kwargs={"pk": pk}).get


@app.patch(
    "/university/pk",
    response=schemes.UniversitySchemeOut,
    tags=["university"]
)
def update_university(request, pk: int, payload: schemes.UniversitySchemeIn):
    obj = orm(UniversityModel, fd_kwargs={"pk": pk}).get
    payload = payload.dict(exclude_none=True)
    return update(obj, payload)


@app.delete(
    "/university/pk",
    response=schemes.Message,
    tags=["university"]
)
def delete_university(request, pk: int):
    obj = orm(UniversityModel, fd_kwargs={"pk": pk}).get
    obj.delete()
    return {"message": "success deleted"}


@app.post(
    "/student/type",
    response=schemes.StudentTypeSchemeOut,
    tags=["student type"]
)
def create_student_type(request, payload: schemes.StudentTypeSchemeIn):
    return StudentTypeModel.objects.create(title=payload.title)


@app.get(
    "/students/type",
    response=list[schemes.StudentTypeSchemeOut],
    tags=["student type"]
)
def get_student_types(request):
    return orm(StudentTypeModel).all


@app.get(
    "/student/type/pk",
    response=schemes.StudentTypeSchemeOut,
    tags=["student type"]
)
def get_student_type(request, pk: int):
    return orm(StudentTypeModel, fd_kwargs={"pk": pk}).get


@app.patch(
    "/student/type/pk",
    response=schemes.StudentTypeSchemeOut,
    tags=["student type"]
)
def update_student_type(request, pk: int, payload: schemes.UniversitySchemeIn):
    obj = orm(StudentTypeModel, fd_kwargs={"pk": pk}).get
    payload = payload.dict(exclude_none=True)
    return update(obj, payload)


@app.delete(
    "/student/type/pk",
    response=schemes.Message,
    tags=["student type"]
)
def delete_student_type(request, pk: int):
    obj = orm(StudentTypeModel, fd_kwargs={"pk": pk}).get
    obj.delete()
    return {"message": "success deleted"}


@app.post(
    "/default/price",
    response=schemes.PriceSchemeOut,
    tags=["default price"]
)
def create_default_price(request, payload: schemes.PriceSchemeIn):
    return DefaultPricesModel.objects.create(price=payload.price)


@app.get(
    "/default/prices",
    response=list[schemes.PriceSchemeOut],
    tags=["default price"]
)
def get_default_prices(request):
    return orm(DefaultPricesModel).all


@app.get(
    "/default/prices/pk",
    response=schemes.PriceSchemeOut,
    tags=["default price"]
)
def get_default_price(request, pk: int):
    return orm(DefaultPricesModel, fd_kwargs={"pk": pk}).get


@app.patch(
    "/default/prices/pk",
    response=schemes.PriceSchemeOut,
    tags=["default price"]
)
def update_default_price(request, pk: int, payload: schemes.PriceSchemeIn):
    obj = orm(DefaultPricesModel, fd_kwargs={"pk": pk}).get
    payload = payload.dict(exclude_none=True)
    return update(obj, payload)


@app.delete(
    "/default/prices/pk",
    response=schemes.Message,
    tags=["default price"]
)
def delete_default_price(request, pk: int):
    obj = orm(DefaultPricesModel, fd_kwargs={"pk": pk}).get
    obj.delete()
    return {"message": "success deleted"}


@app.post(
    "/student",
    response=schemes.StudentSchemeOut,
    tags=["student"]
)
def create_student(request, payload: schemes.StudentSchemeIn):
    payload = payload.dict()
    payload.pop("investors_id")
    password1 = payload.pop("password1", None)
    password2 = payload.pop("password2", None)
    user_dict = payload.pop("user", None)
    if password1 is None or password2 is None:
        raise HttpError(status_code=400, message="password1 and password2 is required")
    elif password1 != password2:
        raise HttpError(status_code=400, message="passwords do not match")
    elif user_dict is None:
        raise HttpError(status_code=400, message="user object is required")
    user_dict["password"] = password1
    user = UserModel.objects.create_user(**user_dict)
    return StudentModel.objects.create(user_id=user.pk, **payload)


@app.get(
    "/students",
    response=list[schemes.StudentSchemeOut],
    tags=["student"],
    exclude_unset=True
)
def get_students(
        request,
        page: Union[int, None] = Query(default=None, description="enter the page number", ge=1),
        tag_id: Union[int, None] = Query(default=None, alias="tag", description="entry tag id"),
        university_id: Union[int, None] = Query(default=None, alias="university", description="entry university id"),
        search: bool = Query(default=False,
                             description="if search is true then there will be a search and not a filtering")
):
    if search is True:
        args = (Q(tag_id=tag_id) | Q(university_id=university_id))
        filter_data_args, q_filtered = remove_null_from_Q_object(args)
        request_orm = check_paginate(StudentModel, page=page, filter_data_args=filter_data_args, q_filtered=q_filtered,
                                     sr_fields=("user", "university", "tag"), pr_fields=("investors",))
        data = request_orm.select_related_and_prefetch_related_and_filter
        return add_count_objects(data, request_orm.count_paginate_Q_filter)
    filter_data = schemes.StudentFilterScheme(tag_id=tag_id, university_id=university_id)
    filter_dict = filter_data.dict(exclude_none=True)
    request_orm = check_paginate(StudentModel, page=page, fd_kwargs=filter_dict,
                                 sr_fields=("user", "university", "tag"), pr_fields=("investors",))
    data = request_orm.select_related_and_prefetch_related_and_filter
    return add_count_objects(data, request_orm.count_paginate_filter)


@app.get(
    "/student/pk",
    response=schemes.StudentSchemeOut,
    tags=["student"]
)
def get_student(request, pk: int):
    return orm(StudentModel, fd_kwargs={"pk": pk}).get


@app.patch(
    "/student/pk",
    response=schemes.StudentSchemeOut,
    tags=["student"]
)
def update_student(request, pk: int, payload: schemes.StudentSchemeIn):
    obj = orm(StudentModel, fd_kwargs={"pk": pk}).get
    payload = payload.dict(exclude_none=True)
    return update(obj, payload)


@app.delete(
    "/default/prices/pk",
    response=schemes.Message,
    tags=["default price"]
)
def delete_student(request, pk: int):
    obj = orm(StudentModel, fd_kwargs={"pk": pk}).get
    obj.delete()
    return {"message": "success deleted"}


@app.post(
    "/investing",
    response=schemes.Message,
    tags=["investing"],
    exclude_unset=True
)
def create_investing(request, payload: schemes.InvestingStudentSchemeIn):
    """
    - **sum**: сумма для инвестиции студента\n
    - **investors**: листь айдишек инвесторов, если 1 и тот-же инвестор плотит 2 раза то будет исключение, и деньги вернутся
    обратно инвестору\n
    - **student**: айди студента который инвестор хочет инвестировать\n
    **Эта функция доконца не доработан, может случиться баги, но основной функционал работает**
    """
    payload = payload.dict()
    investment_amount = payload.get("sum")
    list_investors_id: List = payload.pop("investors", None)
    if list_investors_id is not None and len(list_investors_id) > 0:
        list_pk_investors_in_db: List = list(
            InvestorModel.objects.filter(students__id=payload["student_id"]).values_list("id", flat=True))
        student = get_object_or_404(StudentModel, pk=payload["student_id"])
        for pk in list_investors_id:
            investor = get_object_or_404(InvestorModel, pk=pk)
            if pk in list_pk_investors_in_db:
                investing = InvestingStudentModel.objects.filter(investor_id=investor.id, student_id=student.id)
                if investing.exists():
                    student.save(allotted_amount=student.allotted_amount - investing.last().sum)
                    investor.save(remainder=investor.remainder + investing.last().sum)
                investing.delete()
                student.investors.remove(investor)
                continue
            if investor.remainder < investment_amount:
                raise HttpError(status_code=400, message="insufficient funds")
            InvestingStudentModel.objects.create(investor_id=investor.id, student_id=student.id, sum=investment_amount)
            student.investors.add(investor)
            allotted_amount = student.allotted_amount + investment_amount
            if student.contract_amount < allotted_amount:
                investment_amount = allotted_amount - student.allotted_amount
            investor.save(remainder=investor.remainder - investment_amount)
            student.save(allotted_amount=allotted_amount)
        return schemes.Message(message="success saved !!!")


@app.get(
    "/investments",
    response=List[schemes.InvestingStudentSchemeOut],
    tags=["investing"],
    exclude_unset=True
)
def get_investments(request, page: int = Query(default=None, ge=1)):
    request_orm = check_paginate(InvestingStudentModel, page=page, sr_fields=("investor", "student"))
    data = request_orm.select_related_all
    return add_count_objects(data, request_orm.count_paginate_filter)


@app.get(
    "/investing/pk",
    response=schemes.InvestingStudentSchemeOut,
    tags=["investing"],
    exclude_unset=True
)
def get_investing(request, pk: int = Query(..., description="Введите id иневестиции"), page: int = None):
    data = orm(InvestingStudentModel, fd_kwargs={"pk": pk}, sr_fields=("investor", "student")).select_related_and_get
    return data


@app.get(
    "statistics/",
    response=schemes.StaticsScheme,
    tags=["statistics"]
)
def get_statistics(request, year: int = Query(default=dt.datetime.now().year, ge=2000, description="введите год")):
    amounts = StudentModel.objects.aggregate(
        collected_amounts=Sum("allotted_amount"), requested_amount=Sum("contract_amount"))
    must_be_collected = 0
    if amounts["requested_amount"] >= amounts["collected_amounts"]:
        must_be_collected = amounts["requested_amount"] - amounts["collected_amounts"]
    statistics = {
        "students": StudentModel.objects.filter(created_at__year=year).annotate(
            month=TruncMonth("created_at")).values('month').annotate(count=Count('id')),
        "investors": InvestorModel.objects.filter(created_at__year=year).annotate(
            month=TruncMonth("created_at")).values('month').annotate(count=Count('id')),
    }
    asd = {
        "collected_amounts": amounts["collected_amounts"],
        "requested_amount": amounts["requested_amount"],
        "must_be_collected": must_be_collected,
        "students": list(statistics["students"]),
        "investors": list(statistics["investors"])
    }
    print(asd)
    return {
        "collected_amounts": amounts["collected_amounts"],
        "requested_amount": amounts["requested_amount"],
        "must_be_collected": must_be_collected,
        "students": list(statistics["students"]),
        "investors": list(statistics["investors"])
    }
