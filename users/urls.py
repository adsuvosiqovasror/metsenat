# from django.urls import path
#
# from .views import RegistrationApiView, RefreshAPIView, LoginAPIView, LogoutView, UsersAPIView, ListCreateUserAPIView, RetrieveUpdateDestroyUserAPIView, ListCreateSpecializationAPIView, \
#     UpdateSpecializationAPIView, TeachersAPIView, CustomRetrieveUpdateDestroyTeachersAPIView, ListCreateTeachersAPIView
#
# urlpatterns = [
#     path('register/', RegistrationApiView.as_view()),
#     path('refresh/', RefreshAPIView.as_view()),
#     path('login/', LoginAPIView.as_view()),
#     path('logout/', LogoutView.as_view()),
#     path('profile/', UsersAPIView.as_view()),
#     path('profile/list/', ListCreateUserAPIView.as_view()),  # create and get list Users
#     path('profile/<int:pk>/', RetrieveUpdateDestroyUserAPIView.as_view()),  # admin access only
#     path('specialize/list/', ListCreateSpecializationAPIView.as_view()),  # create and get list specialize
#     path('specialize/<int:pk>/', UpdateSpecializationAPIView.as_view()),
#     path('teacher/', TeachersAPIView.as_view()),
#     path('teacher/list/', ListCreateTeachersAPIView.as_view()),
#     path('teacher/<int:pk>/', CustomRetrieveUpdateDestroyTeachersAPIView.as_view()),
# ]
