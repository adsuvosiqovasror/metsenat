import re
import datetime as dt
from enum import IntEnum
from typing import Union, List, Tuple, Dict

from ninja.errors import HttpError
from pydantic import BaseModel, Field, validator, root_validator
from ninja import Schema

from users.models import UniversityModel, StudentTypeModel, UserModel, DefaultPricesModel


def _validate_phone(phone, saved=True):
    phone = re.sub("[^0-9+]", "", phone)
    if "+" != phone[0]:
        raise HttpError(status_code=400, message="error code \nexample: +998")
    try:
        [int(x) for x in phone[1:]]
    except ValueError:
        raise HttpError(status_code=400, message="error code")
    if saved is True and UserModel.objects.filter(phone=phone).exists():
        raise HttpError(status_code=400, message="user with this number already exists")
    return phone


class Message(Schema):
    message: str


class AccessTokenScheme(BaseModel):
    access: str
    message: str


class LoginSchema(Schema):
    phone: str = Field(min_length=13, max_length=16)
    password: str = Field(..., min_length=8)

    @validator("phone", allow_reuse=True)
    def validate_phone(cls, v):
        return _validate_phone(v, saved=False)


class PriceSchemeIn(Schema):
    price: int


class PriceSchemeOut(Schema):
    id: int
    price: int
    created_at: dt.datetime
    updated_at: dt.datetime


class UniversitySchemeIn(Schema):
    title: str = Field(description="enter university name")


class UniversitySchemeOut(Schema):
    id: int
    title: str = Field(alias="university_name")
    created_at: dt.datetime
    updated_at: dt.datetime


class StudentTypeSchemeIn(Schema):
    title: str = Field(description="enter student type name")


class StudentTypeSchemeOut(Schema):
    id: int
    title: str
    created_at: dt.datetime
    updated_at: dt.datetime


class UserSchemaIn(Schema):
    phone: Union[str, None] = Field(default=None, max_length=17)
    first_name: Union[str, None] = Field(default=None, max_length=155)
    last_name: Union[str, None] = Field(default=None, max_length=155)

    @validator("phone", allow_reuse=True)
    def validate_phone(cls, v):
        return _validate_phone(v)


class UserSchemaOut(Schema):
    id: Union[int, None] = None
    phone: Union[str, None] = None
    first_name: Union[str, None] = None
    last_name: Union[str, None] = None
    created_at: Union[dt.datetime, None] = None
    updated_at: Union[dt.datetime, None] = None


class InvestorTypeChoice(IntEnum):
    legal_entity = 0
    natural_person = 1


class InvestorSchemeIn(BaseModel):
    user: UserSchemaIn
    price_id: Union[int, None] = Field(default=None, alias="price", description="enter price id")
    individual_price: Union[int, None] = Field(default=0, alias="other_amount", description="other amount")
    organization_name: Union[str, None] = Field(default=None, alias="organization", description="organization name")

    # investor_type: InvestorTypeChoice = Field(default=0, alias="type", description="entry investor type, default = 0")

    @root_validator(allow_reuse=True)
    def validate_values(cls, values):
        if values.get("price_id") is None and bool(
                values.get("individual_price") is None or values.get("individual_price") == 0):
            raise HttpError(status_code=400, message="individual_price and price fields, one of them must be filled")
        return values


class InvestorSchemeUpdate(Schema):
    user: UserSchemaIn = None
    price_id: Union[int, None] = Field(default=None)
    individual_price: Union[int, None] = Field(default=None, alias="other_amount", description="other amount")
    organization_name: Union[str, None] = Field(default=None, alias="organization", description="organization name")
    investor_type: Union[int, None] = Field(default=None, ge=0, le=1, description="entry investor type, default = 0")
    moderator: Union[int, None] = None
    password1: Union[str, None] = Field(default=None, min_length=8)
    password2: Union[str, None] = Field(default=None, min_length=8)

    @root_validator(allow_reuse=True)
    def check_password(cls, values):
        password1 = values.get("password1", None)
        password2 = values.get("password2", None)
        if password1 is not None and password2 is not None and password1 != password2:
            raise HttpError(status_code=400, message="passwords do not match")
        return values

    @validator("price_id", allow_reuse=True)
    def validate_price(cls, v):
        if not DefaultPricesModel.objects.filter(pk=v).exists():
            raise HttpError(status_code=400, message="price not found")
        return v


class InvestorSchemeOut(Schema):
    id: int = None
    user: UserSchemaOut = None
    other_amount: Union[int, None] = Field(default=None, alias="individual_price", description="other amount")
    organization: Union[str, None] = Field(default=None, alias="organization_name", description="organization name")
    price: Union[PriceSchemeOut, int, None] = Field(default=None)
    investor_type: Union[int, None] = None
    moderator: Union[int, None] = None
    remainder: Union[int, None] = None
    created_at: dt.datetime = None
    updated_at: dt.datetime = None
    count: int = None


class StudentSchemeIn(Schema):
    user: UserSchemaIn
    university_id: int = Field(alias="university", description="entry university id")
    tag_id: int = Field(alias="tag", description="entry tag id")
    contract_amount: int = Field(description="Please enter the amount contract")
    investors_id: Union[List[int], None] = Field(default=[], alias="investor", description="entry investor id")
    password1: Union[str, None] = Field(default=None, min_length=8)
    password2: Union[str, None] = Field(default=None, min_length=8)

    @root_validator(allow_reuse=True)
    def validate_values(cls, values):
        if not UniversityModel.objects.filter(pk=values.get("university_id")).exists():
            raise HttpError(status_code=400, message="tag not found")
        elif not StudentTypeModel.objects.filter(pk=values.get("tag_id")).exists():
            raise HttpError(status_code=400, message="tag not found")
        return values


class StudentSchemeOut(Schema):
    user: Union[UserSchemaOut, None] = None
    university: Union[UniversitySchemeOut, dict, int, None] = None
    tag: Union[StudentTypeSchemeOut, dict, int, None] = None
    contract_amount: Union[int, None] = None
    allotted_amount: int = Field(default=0)
    investors: Union[List[InvestorSchemeOut], None] = Field(default=None)
    created_at: Union[dt.datetime, None] = None
    updated_at: Union[dt.datetime, None] = None
    count: Union[int, None] = None


class InvestingStudentSchemeIn(Schema):
    sum: Union[int, None]
    investors: Union[List[int], None]
    student_id: Union[int, None] = Field(alias="student")


class InvestingStudentSchemeOut(Schema):
    id: Union[int, None] = None
    sum: Union[int, None] = None
    investor: Union[InvestorSchemeOut, dict, int, None] = None
    student: Union[StudentSchemeOut, dict, int, None] = None
    created_at: Union[dt.datetime, None] = None
    updated_at: Union[dt.datetime, None] = None
    count: Union[int, None] = None


class InvestorsFilterScheme(BaseModel):
    moderator: Union[int, None] = None
    price_id: Union[int, None] = None
    individual_price__range: Union[str, None] = Field(default=None, alias="prices")
    created_at__date__range: Union[Tuple[dt.date, dt.date], Tuple[None, None]] = Field(alias="range_date")

    @root_validator(allow_reuse=True)
    def validate_values(cls, values):
        prices: str = values.get("individual_price__range")
        date = values.get("created_at__date__range")
        values["created_at__date__range"] = None
        values["individual_price__range"] = None
        if values.get("price_id") is not None and values.get("individual_price__range") is not None:
            raise HttpError(status_code=400,
                            message="you cannot pass price_id and Individual_prices arguments at the same time")
        elif type(prices) is str:
            list_prices_str: List = re.sub("[^0-9-]", "", prices).split("-")
            if len(list_prices_str) != 2:
                raise HttpError(status_code=400,
                                message="attribute individual_prices filled out incorrectly, example: 15000-30000")
            try:
                list_prices_int = [int(num) for num in list_prices_str]
                list_prices_int.sort()
            except ValueError:
                HttpError(status_code=400,
                          message="attribute individual_prices filled out incorrectly, example: 15000-30000")
            values["individual_price__range"] = (list_prices_int[0], list_prices_int[1])
        # 2022 - 09 - 22
        if type(date[0]) is dt.date and type(date[1]) is dt.date and len(date) == 2:
            values["created_at__date__range"] = date
        return values


class StudentsInvestorsStatics(Schema):
    month: Union[dt.date, None] = None
    count: Union[int, None] = None


class StaticsScheme(BaseModel):
    collected_amounts: int
    requested_amount: int
    must_be_collected: int
    students: List[StudentsInvestorsStatics] = None
    investors: List[StudentsInvestorsStatics] = None


class StudentFilterScheme(BaseModel):
    tag_id: Union[int, None] = None
    university_id: Union[int, None] = None

# class ProfileSchema(UserSchema):
#     manager: Optional[ManagerSchemaOut]
#     student: Optional[StudentSchemaOut]
#     teacher: Optional[TeacherSchemaOut]
#
#     @validator('manager', 'student', 'student', allow_reuse=True)
#     def chek_obj(cls, v):
#         if v.user is not None:
#             del v.user
#         elif v is None:
#             del v
#         return v
