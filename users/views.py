# from abc import ABC
#
# from django.contrib.auth import authenticate
# from django.core.cache import cache
# from django.utils.translation import gettext_lazy as _
# from rest_framework import status, authentication
# from rest_framework.exceptions import ValidationError, AuthenticationFailed
# from rest_framework.generics import GenericAPIView
# from rest_framework.permissions import AllowAny
# from rest_framework.response import Response
# from rest_framework.views import APIView
#
# from base.cache import AuthUserCache
# from base.generics import CustomRetrieveUpdateDestroyAPIView, CustomListCreateAPIView, CustomUpdateAPIView
# from users.models import UserModel, UserProfileModel, Jwt, SpecializationModel, TeachersModel
# from users.serializers import UserSerializer, LoginSerializer, ProfileSerializer, SpecializationSerializer, TeachersSerializer
# from utils.backends import JWTAuthentication
# from utils.permissions import IsAuthenticatedCustom
# from utils.token import UpdateTokens
#
#
# class RegistrationApiView(APIView):
#     permission_classes = [AllowAny]
#     serializer_class = UserSerializer
#
#     def post(self, request):
#         serializer = self.serializer_class(data=request.data, required=False)
#         if serializer.is_valid(raise_exception=False):
#             data = serializer.validated_data
#             password1 = data.pop('password1', False)
#             password2 = data.pop('password2', False)
#             if not password2 or not password2 or password2 != password1:
#                 return Response({'error': 'passwords do not match'}, status=400)
#             data['password'] = password1
#             serializer.create(data)
#             user = UserModel.objects.get(email=data['email'])
#             UserProfileModel(user=user).save()
#             self.del_cache()
#             return Response(data=serializer.data, status=status.HTTP_201_CREATED)
#         data = serializer.data
#         if UserModel.objects.filter(username=data['username'], email=data['email']).exists():
#             return Response({'error': 'A user with the same email or username already exists'},
#                             status=status.HTTP_401_UNAUTHORIZED)
#         return Response({'error': serializer.errors}, status=status.HTTP_401_UNAUTHORIZED)
#
#     def del_cache(self):
#         key = set(cache.keys('UserProfile*'))
#         key_pk = set(cache.keys('UserProfilepk*'))
#         cache.delete_many(list(key.difference(key_pk)))
#
#
# class LoginAPIView(APIView):
#     permission_classes = [AllowAny]
#     serializer_class = LoginSerializer
#
#     def post(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.data)
#         if serializer.is_valid(raise_exception=False):
#             data = serializer.data
#             request.user = authenticate(request, email=data['email'], password=data['password'])
#             if request.user is None:
#                 return Response({'error': 'User not found!'}, status=401)
#             AuthUserCache().del_cache_user(request.user.jwt.access)
#             return UpdateTokens(request=request).create_access_refresh_token
#         return Response({'error': 'data not valid'}, status=status.HTTP_401_UNAUTHORIZED)
#
#
# class LogoutView(APIView):
#     permission_classes = [IsAuthenticatedCustom]
#
#     def get(self, request):
#         user_id = request.user.id
#         Jwt.objects.filter(user_id=user_id).delete()
#         response = Response()
#         response.delete_cookie(key='_at')
#         response.status_code = 200
#         response.data = "logged out successfully"
#         return response
#
#
# class RefreshAPIView(GenericAPIView):
#
#     def get(self, request, *args, **kwargs):
#         request.META.set('HTTP_AUTHORIZATION', f"bearer {self.request.COOKIES.get('_at','!@$%^')}".encode())
#         try:
#             user, exc = JWTAuthentication().authenticate(self.request)
#         except Exception:
#             msg = _('Invalid refresh token. No credentials provided.')
#             raise AuthenticationFailed(msg)
#         return UpdateTokens(self.request).response
#
#
# class UsersAPIView(CustomRetrieveUpdateDestroyAPIView, ABC):
#     permission_classes = [IsAuthenticatedCustom]
#     # permission_classes = [AllowAny]
#     serializer_class = ProfileSerializer
#     queryset = UserProfileModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_filtered = self.select_related_and_filterd
#
#     def get_pk(self):
#         self.kwargs['pk'] = self.request.user.profile.pk
#         super().get_pk()


# class ListCreateUserAPIView(CustomListCreateAPIView):
#     permission_classes = [AllowAny]
#     serializer_class = ProfileSerializer
#     queryset = UserProfileModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     select_related_fields = ('user', )
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_all = self.select_related_all
#
#
# class RetrieveUpdateDestroyUserAPIView(CustomRetrieveUpdateDestroyAPIView):
#     permission_classes = [AllowAny]
#     serializer_class = ProfileSerializer
#     queryset = UserProfileModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     select_related_fields = ('user', )
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_filtered = self.select_related_and_filterd
#
#
# class ListCreateSpecializationAPIView(CustomListCreateAPIView, ABC):
#     permission_classes = [AllowAny]
#     serializer_class = SpecializationSerializer
#     queryset = SpecializationModel
#
#
# class UpdateSpecializationAPIView(CustomUpdateAPIView, ABC):
#     permission_classes = [AllowAny]
#     serializer_class = SpecializationSerializer
#     queryset = SpecializationModel
#
#
# class TeachersAPIView(CustomRetrieveUpdateDestroyAPIView):
#     permission_classes = [AllowAny]
#     serializer_class = TeachersSerializer
#     queryset = TeachersModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     select_related_fields = ('user', )
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_filtered = self.select_related_and_filterd
#
#     def get_pk(self):
#         self.filter_data = {'pk': self.request.user.pk}
#
#
# class CustomRetrieveUpdateDestroyTeachersAPIView(CustomRetrieveUpdateDestroyAPIView):
#     permission_classes = [AllowAny]
#     serializer_class = TeachersSerializer
#     queryset = TeachersModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     select_related_fields = ('user', )
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_filtered = self.select_related_and_filterd
#
#
# class ListCreateTeachersAPIView(CustomListCreateAPIView, ABC):
#     permission_classes = [AllowAny]
#     serializer_class = TeachersSerializer
#     queryset = TeachersModel
#     serializer_context = {'update_profile': True, 'fk_or_oo_fields': ['user']}
#     select_related_fields = ('user', )
#     customize_bool = True
#
#     def custom_func(self):
#         self.default_orm_request_filtered = self.select_related_and_filterd
