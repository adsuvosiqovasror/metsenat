from django.contrib import admin

from .models import UserModel, JwtModel, StudentModel, UniversityModel, StudentTypeModel, DefaultPricesModel, \
    InvestorModel, InvestingStudentModel


@admin.register(UserModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', "permission", "last_name")  # 'username',
    list_display_links = ('phone', "id")
    list_filter = ('is_active',)
    search_fields = ('phone', 'first_name', 'last_name')


@admin.register(UniversityModel)
class UniversityAdmin(admin.ModelAdmin):
    list_display = ('pk', 'university_name', 'created_at')
    list_display_links = ('pk', 'created_at',)


@admin.register(StudentTypeModel)
class StudentTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created_at')
    list_display_links = ('id', 'created_at')


@admin.register(StudentModel)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'contract_amount', "allotted_amount", "created_at")  # 'username',
    list_display_links = ("id",)


@admin.register(DefaultPricesModel)
class DefaultPricesAdmin(admin.ModelAdmin):
    list_display = ('id', 'price', 'created_at')
    list_display_links = ('id', 'created_at')


@admin.register(JwtModel)
class JwtAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'user_id')
    list_display_links = ('user',)


@admin.register(InvestorModel)
class InvestorAdmin(admin.ModelAdmin):
    list_display = ('id', 'organization_name', "investor_type", "moderator")  # 'username',
    list_display_links = ("id",)
    search_fields = ('organization_name', 'individual_price')


@admin.register(InvestingStudentModel)
class InvestingStudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'sum', "created_at")  # 'username',
    list_display_links = ("id", "created_at")

